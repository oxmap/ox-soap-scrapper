import {Genre} from './genre.model';

export class GenresListModel {

  public genres: Array<Genre>;

}
