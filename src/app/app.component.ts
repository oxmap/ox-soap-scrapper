import { Component } from '@angular/core';
import {AuthService} from './core/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  isLoggedIn: boolean;

  constructor(public auth: AuthService) {

    // this.auth.afAuth.authState.subscribe(
    //   a => {
    //     this.isLoggedIn = a !== null;
    //   }
    // );

  }
}
