import { Component, OnInit } from '@angular/core';
import {MoviesService} from '../../../services/inTheater/movies.service';
import {PaginatorModel} from '../../../models/paginator.model';
import {MovieModel} from '../../../models/movie.model';
import {GenresListModel} from "../../../models/genres-list.model";

@Component({
  selector: 'app-all-movies',
  templateUrl: './all-movies.component.html',
  styleUrls: ['./all-movies.component.scss']
})
export class AllMoviesComponent implements OnInit {

  movies: Array<MovieModel> = [];
  curType: {title: string, value: number} = {title: 'Сегодня в кино', value: 0};
  types = [
    {title: 'Сегодня в кино', value: 0},
    {title: 'Скоро в кино', value: 1},
    {title: 'Популярные', value: 2}
  ];

  genres: GenresListModel;
  max = 10;
  min = 0;
  step = 0.1;
  value = 0;
  thumbLabel = true;
  tickInterval = 10;
  disabled = false;

  constructor(private moviesService: MoviesService) { }

  ngOnInit() {
    this.getNowPlayinMovies(1);
    this.moviesService.getGenres().subscribe(res => {
        this.genres = res.genres;
      }
    );
  }

  getNowPlayinMovies(page: number) {
    this.moviesService.getNowPlaying(page).subscribe(res => {
      this.movies = res.results;
      this.movies.forEach(np => np['isMovie'] = true);
    });
  }

  getUpComingMovies(page: number) {
    this.moviesService.getUpComingMovies(page).subscribe(res => {
      this.movies = res.results.filter(up => new Date(up.release_date).getTime() >= new Date().getTime());
      this.movies.forEach(np => np['isMovie'] = true);
    });
  }

  getPopularMovies(page: number) {
    this.moviesService.getPopular(page).subscribe(res => {
      this.movies = res.results;
      this.movies.forEach(np => np['isMovie'] = true);
    });
  }

  changeType(e): void {
    switch(e.value.value) {
      case(0): {
        this.getNowPlayinMovies(1);
        break;
      }
      case(1): {
        this.getUpComingMovies(1);
        break;
      }
      case(2): {
        this.getPopularMovies(1);
        break;
      }
      default: {
        break;
      }
    }
  }

  getMoviesByGenre(id) {
    this.moviesService.getMoviesByGenre(id).subscribe( res => {
      this.movies = res.results;
      this.movies.forEach(np => np['isMovie'] = true);
    }, error => console.error);
  }


  setVote(e): void {
    this.movies.filter(movie => movie.vote_average == e.value);
  }
}
