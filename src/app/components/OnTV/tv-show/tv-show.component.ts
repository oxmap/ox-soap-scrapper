import { Component, OnInit } from '@angular/core';
import {TvShowModel} from '../../../models/onTV/tvShow.model';
import {Image} from '../../../models/image.model';
import {Cast} from '../../../models/cast.model';
import {OnTVService} from '../../../services/onTV/onTV.service';
import {ActivatedRoute} from '@angular/router';
import {PaginatorModel} from '../../../models/paginator.model';

@Component({
  selector: 'app-tv-show',
  templateUrl: './tv-show.component.html',
  styleUrls: ['./tv-show.component.scss']
})
export class TvShowComponent implements OnInit {

  tvShow: TvShowModel;
  cast: Cast;
  similarShows: Array<PaginatorModel> = [];
  images: Array<Image> = []
  isLoading = true;

  constructor(
    private onTvService: OnTVService,
    private router: ActivatedRoute
  ) { }

  ngOnInit() {
    this.router.params.subscribe( (params) => {
      const id = params['id'];

      this.onTvService.getTVShow(id).subscribe( tvShow => {
        this.tvShow = tvShow;

        if (!this.tvShow) {
          alert('Server Error')
        } else {
          this.isLoading = false;
        }
      });

      this.onTvService.getImages(id).subscribe( res => {
        this.images = res.backdrops;
        this.images.push(...res.posters);
      });

      this.onTvService.getCredits(id).subscribe( res => {
        res.cast = res.cast.filter( item => { return item.profile_path });
        this.cast = res.cast.slice(0, 5);
      });

      this.onTvService.getRecomended(id).subscribe(res => {
        this.similarShows = res.results.slice(0, 12);
        this.similarShows.forEach(np => np['isMovie'] = true);
      });

    });
  }

}
