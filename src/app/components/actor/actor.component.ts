import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {MoviesService} from '../../services/inTheater/movies.service';
import {Cast} from '../../models/cast.model';
import {Image} from '../../models/image.model';
import {Actor} from '../../models/actor.model';

@Component({
  selector: 'app-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.scss']
})
export class ActorComponent implements OnInit {

  person: Actor = new Actor();
  cast: Cast;
  externalIds: Object = {};
  images: Array<Image> = [];


  constructor(
    private _moviesService: MoviesService,
    private router: ActivatedRoute
  ) { }

  ngOnInit() {
    this.router.params.subscribe((params) => {
      const id = params['id'];
      this._moviesService.getPersonDetail(id).subscribe(person => {
        this.person = person;
      }, error => console.error);

      this._moviesService.getPersonCast(id).subscribe(res => {
        // res.cast = res.cast.filter( item => { return item.profile_path });
        this.cast = res.cast.slice(0, 5);
      }, error => console.error);

      this._moviesService.getPersonExternalData(id).subscribe(res => {
        this.externalIds = res;
      }, error => console.error);

      this._moviesService.getPersonImages(id).subscribe( res => {
        console.log(res);
        this.images = res.profiles;
      });

    });
  }

}
